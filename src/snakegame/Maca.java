/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snakegame;

import java.util.Random;
import javax.swing.ImageIcon;

/**
 *
 * @author Lucas
 */
public class Maca {

    private int largura;
    private int altura;
    private int x;
    private int y;
    private ImageIcon img;

    public Maca() {

    }

    public Maca(String url) {
        Random r = new Random();
        img = new ImageIcon(url);
        largura = img.getIconWidth();
        altura = img.getIconHeight();
        x = r.nextInt(20) * 30;
        y = r.nextInt(20) * 30;
    }

    public int getLargura() {
        return largura;
    }

    public void setLargura(int largura) {
        this.largura = largura;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public ImageIcon getImg() {
        return img;
    }

    public void setImg(ImageIcon img) {
        this.img = img;
    }

    public void geraLocal() {
        Random r = new Random();
        this.x = (int) (Math.random() * 20) * 30;
        this.y = (int) (Math.random() * 20) * 30;
    }

        // Método para checar se a cobrinha comeu a comida
    public void checarComida(Cobra cobra, Jogador player) {
        
         // Se ele comer na mesma posição (x,y) então aumenta o corpo da cobrinha
        // aumenta a pontuação e gera uma nova comida
        // Se ele comer na mesma posição (x,y) então aumenta o corpo da cobrinha
        // aumenta a pontuação e gera uma nova comida
        if ((cobra.getPontocobra(0).getX() == this.getX()) && (cobra.getPontocobra(0).getY() == this.getY())) {
            cobra.setPontos(cobra.getPontos()+1);
            player.setPontuacao(player.getPontuacao()+1);
            cobra.addPontoCobra(new Pontos());
            geraLocal();
        }
    }

}
