/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snakegame;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

/**
 *
 * @author Lucas
 */
public class Jogador {

    private int pontuacao;
    private boolean estaJogando;

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }
    private int nivel;
    public Jogador()
    {
        
        estaJogando = true;
    }
    public boolean isEstaJogando() {
        return estaJogando;
    }

    public void setEstaJogando(boolean estaJogando) {
        this.estaJogando = estaJogando;
    }
    
    public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }
    
        public void desenharPontuacao (Graphics g, Grade grade)
    {
        // Define a frase para escrever
        String SCORE = ""+this.getPontuacao();
        
        Font SCORE_FONT = new Font("Arial", Font.BOLD, 15);
        // Define o tamanho da fonte
        FontMetrics SCORE_METRICA = g.getFontMetrics(SCORE_FONT);

        // Define a cor da fonte
        g.setColor(Color.BLACK);
        // Seta a fonte para o gráfico
        g.setFont(SCORE_FONT);
        // Desenha a fonte na tela
        g.drawString(SCORE, (SCORE_METRICA.stringWidth(SCORE)) , 600-10);
    }
        
    public void FimDeJogo (Graphics g, Grade grade)
    {
        // Define a frase para escrever
        String msg = "GAME OVER, SUA PONTUAÇÃO: " + this.getPontuacao();
        // Define o estilo da fonte
        Font pequena = new Font("ARIAL", Font.BOLD, 14);
        // Define o tamanho da fonte
        FontMetrics metrica = g.getFontMetrics(pequena);

        // Define a cor da fonte
        g.setColor(Color.BLACK);
        // Seta a fonte para o gráfico
        g.setFont(pequena);
        // Desenha a fonte na tela
        g.drawString(msg, (grade.WIDTH_ - metrica.stringWidth(msg)) / 2, grade.HEIGHT_ / 2);
    }
    
      public void mover (Cobra cobra, Grade grade)
    {
        // Para cada ponto da cobrinha desenha em (x,y)
        for (int i = cobra.getPontos() ; i > 0; i--)
        {
            cobra.getPontocobra(i).setX(cobra.getPontocobra(i-1).getX());
            cobra.getPontocobra(i).setY(cobra.getPontocobra(i-1).getY()); 
          
        }

        // Se for para esquerda decrementa em x
        if (grade.isLeft())
        {
            cobra.getPontocobra(0).setX(cobra.getPontocobra(0).getX() - grade.TAMANHO_PONTO); 
        }

        // Se for para direita incrementa em x
        if (grade.isRight())
        {
            cobra.getPontocobra(0).setX(cobra.getPontocobra(0).getX() + grade.TAMANHO_PONTO); 
        }

        // Se for para cima decrementa em y
        if (grade.isUp())
        {
            cobra.getPontocobra(0).setY(cobra.getPontocobra(0).getY() - grade.TAMANHO_PONTO); 
        }

        // Se for para baixo incrementa em y
        if (grade.isDown())
        {
            cobra.getPontocobra(0).setY(cobra.getPontocobra(0).getY() + grade.TAMANHO_PONTO); 
        }
        
    }


}
