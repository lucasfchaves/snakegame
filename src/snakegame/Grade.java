
package snakegame;


// Importações necessárias para rodar o jogo
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JPanel;
import javax.swing.Timer;


public class Grade extends JPanel implements Runnable
{
    // Tamanho do JPanel em Largura x Altura
    public final int WIDTH_ = 600;
    public final int HEIGHT_ = 600;
    // Tamanho de cada ponto na tela
    public final int TAMANHO_PONTO = 30;
    // Tamanho total de pontos, multiplicando a largura e altura
    public final int TODOS_PONTOS = 400;
    // Um delay para o tempo de execução do jogo
    private  int DELAY = 80;


    // Definição dos movimentos
    private boolean left = false;
    private boolean right = false;
    private boolean up = false;
    private boolean down = false;

    private boolean restart = false;
    public boolean isLeft() {
        return left;
    }

    public boolean isRight() {
        return right;
    }

    public boolean isUp() {
        return up;
    }

    public boolean isDown() {
        return down;
    }


    
    private Maca maca = new Maca("comida.png");
    private Cobra cobra = new Cobra("head30x30", "corpo30x30",600,600);
    private Jogador player = new Jogador();
    
    // Método construtor da classe
    public Grade (int nivel)
    {
        player.setNivel(nivel);
        DELAY = 150-(nivel*50);
        // Cria uma instrução de teclado
        addKeyListener(new TAdapter());

        // Seta o plano de fundo como preto
        setBackground(Color.WHITE);

        // Define o foco para o JPanel
        setFocusable(true);
        // Define o tamanho da tela
        setSize(WIDTH_, HEIGHT_);

        // Inicializa do jogo
        Thread t = new Thread(this);
        t.start();
       // initJogo();
    }

    // Método para inicializar o jogo
    public void initJogo()
    {
       

        // Define a posição em (x,y) de cada ponto
        for (int i = 0; i < cobra.getPontos()+1; i++)
        {
            cobra.addPontoCobra(new Pontos(120 - i*30, 120));
         
        }

        // Gera a primeira comida
        maca.geraLocal();

              // Inicia o tempo de execução do jogo

        
  
    }

    // Método para desenhar elementos na tela do jogo
    @Override
    public void paint (Graphics g)
    {
        // Define o atribuito para a classe própria
        super.paint(g);

        // Analisa se o jogo esta em andamento, se estiver desenha na tela,
        // se não estiver, o jogo é dado como o fim
        if (player.isEstaJogando() || restart==true)
        {
            
            // Desenha a comida no plano (x,y) do jogo
            g.drawImage(maca.getImg().getImage(), maca.getX(), maca.getY(), this);

            // Para cada ponto da cobrinha, desenha a cabeça e o corpo
            // em (x,y)
            for (int i = 0; i < cobra.getPontos(); i++)
            {
                if (i == 0){
                    if (up)
                 g.drawImage(cobra.getImagecabeca(cobra.UP), cobra.getPontocobra(i).getX(), cobra.getPontocobra(i).getY(), this); 
                    else if(down)
                 g.drawImage(cobra.getImagecabeca(cobra.DOWN), cobra.getPontocobra(i).getX(), cobra.getPontocobra(i).getY(), this); 
                    else if(left)
                 g.drawImage(cobra.getImagecabeca(cobra.LEFT), cobra.getPontocobra(i).getX(), cobra.getPontocobra(i).getY(), this); 
                    else if(right)
                 g.drawImage(cobra.getImagecabeca(cobra.RIGHT), cobra.getPontocobra(i).getX(), cobra.getPontocobra(i).getY(), this);
                    }
                else
                { g.drawImage(cobra.getImagecorpo(0), cobra.getPontocobra(i).getX(), cobra.getPontocobra(i).getY(), this); }
            }

            // Desenha a pontuação na tela
            player.desenharPontuacao(g, this);

            // Executa a sincronia de dados
            Toolkit.getDefaultToolkit().sync();

            // Pausa os gráficos
            g.dispose();
        }
        else if(restart==false)
        {
            
            // Executa o fim de jogo
            player.FimDeJogo(g, this);
            
                
             Toolkit.getDefaultToolkit().sync();
             
             
            g.dispose();

            
                    
        }
        
    }

    // Método de ações durante a execução do jogo
    public void actionPerformed (ActionEvent e)
    {
       

    }

    @Override
    public void run() {
       initJogo();
              
       
       while (true)
        { // System.out.println("TESTE");
           
            if(player.isEstaJogando() && restart == false){
            maca.checarComida(cobra, player);
            cobra.checarColisão(player,this);
            player.mover(cobra,this);
            
             repaint();
               try {
                Thread.sleep(120-(player.getNivel()*35));
                } catch (InterruptedException ex) {
            }    
             
            }
            else if (restart == true) 
            {
                
                restart = false;
                    left = false;
                    right = false;
                    up = false;
                    down = false;

                this.maca = new Maca("comida.png");
                this.cobra = new Cobra("head30x30", "corpo30x30",600,600);
                this.player = new Jogador();
                player.setEstaJogando(true);
                run();
             /*   repaint();
                initJogo();
                
          
            maca.checarComida(cobra, player);
            cobra.checarColisão(player,this);
            player.mover(cobra,this);*/
            
             
                        
            }
                
        }

      
    }

    // Classe para analisar o teclado
    private class TAdapter extends KeyAdapter
    {

        // Método para verificar o que foi teclado
        @Override
        public void keyPressed (KeyEvent e)
        {
            // Obtém o código da tecla
            int key =  e.getKeyCode();

            // Verifica os movimentos e manipula as variáveis, para movimentar
            // corretamente sobre a tela
            if ((key == KeyEvent.VK_R))
            {
                restart=true;
                
            }
            if ((key == KeyEvent.VK_LEFT) && (!right))
            {
                left = true;
                up = false;
                down = false;
            }

            if ((key == KeyEvent.VK_LEFT) && (!right))
            {
                left = true;
                up = false;
                down = false;
            }

            if ((key == KeyEvent.VK_RIGHT) && (!left))
            {
                right = true;
                up = false;
                down = false;
            }

            if ((key == KeyEvent.VK_UP) && (!down))
            {
                up = true;
                left = false;
                right = false;
            }

            if ((key == KeyEvent.VK_DOWN) && (!up))
            {
                down = true;
                left = false;
                right = false;
            }
        }
    }
    
 
}