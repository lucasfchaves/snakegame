/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snakegame;

import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.ImageIcon;

/**
 *
 * @author Lucas
 */
public class Cobra {
    
  public static final int
    UP       = 0,
    DOWN     = 1,
    LEFT    = 2,
    RIGHT    = 3;
  
    private int largura;
    private int altura;
    private int x;
    private int y;
    private int pontos;
    private ImageIcon img[] = new ImageIcon[4];
    private ImageIcon imgcorpo[] = new ImageIcon[2];
    private Image imagecabeca[] = new Image[4];
    private Image imagecorpo[] = new Image[2];
    private ArrayList<Pontos> pontoscobra = new ArrayList();

    
    public Pontos getPontocobra(int i)
    {
        return (Pontos) pontoscobra.get(i);
    }
    
    public void addPontoCobra(Pontos p)
    {
        pontoscobra.add(p);
    }
    
    public ImageIcon getImg(int i) {
        return img[i];
    }

    public ImageIcon getImgcorpo(int i) {
        return imgcorpo[i];
    }
    public Image getImagecabeca(int i) {
        return imagecabeca[i];
    }

    public Image getImagecorpo(int i) {
        return imagecorpo[i];
    }

    
    
    public Cobra() {

    }

    public Cobra(String url, String urlcorpo, int largura, int altura) {
        Random r = new Random();
        img[UP] = new ImageIcon(url+"u.png");
        img[DOWN] = new ImageIcon(url+"d.png");
        img[LEFT] = new ImageIcon(url+"l.png");
        img[RIGHT] = new ImageIcon(url+"r.png");

        
        imgcorpo[UP] = new ImageIcon(urlcorpo+"v.png");
        imgcorpo[DOWN] = new ImageIcon(urlcorpo+"h.png");
        
        largura = img[UP].getIconWidth();
        altura = img[UP].getIconHeight();
        x = r.nextInt(20) * 30;
        y = r.nextInt(20) * 30;
        pontos = 3;
        imagecabeca[UP] = img[UP].getImage();
        imagecabeca[DOWN] = img[DOWN].getImage();
        imagecabeca[LEFT] = img[LEFT].getImage();
        imagecabeca[RIGHT] = img[RIGHT].getImage();
        
        
        imagecorpo[UP] = imgcorpo[UP].getImage();
        imagecorpo[DOWN] = imgcorpo[DOWN].getImage();
    }
    
  
 

   public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }
    public int getLargura() {
        return largura;
    }

    public void setLargura(int largura) {
        this.largura = largura;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }



    public void geraLocal() {
        Random r = new Random();
        this.x = (int) (Math.random() * 20) * 30;
        this.y = (int) (Math.random() * 20) * 30;
    }


        public void checarColisão (Jogador player, Grade grade)
    {
        // Para cada ponto, verifica se este está em posição com outro ponto
        // se estiver ele avista que o jogador parou de jogar devido a colisão
        for (int i = player.getPontuacao(); i > 0; i--)
        {
            if ((this.getPontocobra(0).getX() == this.getPontocobra(i).getX()) && (this.getPontocobra(0).getY() == this.getPontocobra(i).getY()))
            {
                
                player.setEstaJogando(false);
            
            }
            
        }

        // Verifica se a cabeça da cobrinha encostou em algum ponto (x,y)
        // nas bordas (width,height) da tela
        if (this.getPontocobra(0).getY() > grade.HEIGHT_)
        { player.setEstaJogando(false); }

        if (this.getPontocobra(0).getY() < 0)
        {player.setEstaJogando(false); }

        if (this.getPontocobra(0).getX() > grade.WIDTH_)
        {player.setEstaJogando(false); }

        if (this.getPontocobra(0).getX() < 0)
        {player.setEstaJogando(false); }
    }

}
